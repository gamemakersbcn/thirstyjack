﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeerClick : MonoBehaviour {
    Animator beerAnimator;

    private void Start()
    {
        beerAnimator = GetComponent<Animator>();
    }

    private void OnMouseDown() {
        beerAnimator.SetBool("broken", true);
    }

    private void Update()
    {
        if (beerAnimator.GetCurrentAnimatorStateInfo(0).IsName("BeerDestroyed")) {
            Destroy(gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeerMovement : MonoBehaviour {
    private float speed = 3.5f;

	void Update () {
        transform.Translate(-Time.deltaTime * speed, 0, 0);
	}

    private void OnMouseDown() {
        speed = 0f;
    }
}
